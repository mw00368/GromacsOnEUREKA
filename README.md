# GromacsOnEUREKA (GROMAX)

GromacsOnEUREKA is a repository of useful scripts and documentation for using GROMACS for molecular dynamics simulations on the University of Surrey's "EUREKA" HPC cluster.

This page includes installation information, and descriptions/usage of functions included. A step-by-step guide for MD in Gromacs on EUREKA can be found in the [wiki](https://gitlab.eps.surrey.ac.uk/mw00368/GromacsOnEUREKA/-/wikis/home).

*   [Installation](#installation)
*   [Usage](#usage)
    -   [Load Gromacs](#load-gromacs)
    -   [Interface with Gromacs](#interface-with-gromacs)
    -   [Prepare PDBs for Gromacs](#interface-with-gromacs)
    -   [Analysis using AMP](#analysis-using-amp)
    -   [Example with 3PJR](#example-with-3pjr)

## Installation

Install the requirements for the use-cases you deem necessary.

### Requirements for general use

*   Clone and set up [MShTools](https://github.com/mwinokan/MShTools)

### Requirements for use within ASE

*   Install ASE
*   Clone and set up [MPyTools](https://github.com/mwinokan/MPyTools)
*   Clone and set up [AseMolPlot](https://github.com/mwinokan/AseMolPlot)

### Installation

*   Clone this repository `cd; git clone https://gitlab.eps.surrey.ac.uk/mw00368/GromacsOnEUREKA`
*   Add the following to your `~/.bash_profile`:
    -   `export GROMAX="/path/to/GromacsOnEUREKA"`
    -   `export PATH=$PATH:$GROMAX`

## Usage

### Load Gromacs

Source `load_gmx.sh` to load and set up Gromacs:

`source load_gmx.sh`

This is for both interactive sessions and scripts.

### Interface with Gromacs 

Functions to make Gromacs on EUREKA simpler can be found in `gmx_funcs.sh`. Source to use the functions:

`source gmx_funcs.sh`

#### The "gromax" wrapper for Gromacs

Included in `gmx_funcs.sh` is a function called `gromax` which adds additional input arguments, logging (console redirection), verbosity, and error catching to the Gromacs executable.

For example calling:

`gromax grompp -l 5 -f md.mdp -c npt.gro -t npt.cpt -p topol.top -o md_0_1.tpr`

will write to the console:

`Running grompp 5...`

then will call Gromacs for you and upon completion:

`Running grompp 5... Done, exit code: 0 Fri 20 Mar 17:50:43 GMT 2020`.

With the full Gromacs output and error log being written to `_grompp5.log`. If the exit code is non-zero the script will be stopped to prevent further errors and wasting HPC resources on processes that are doomed from the start.

#### Plotting xvg's produced by "gromax energy"

`gmx_funcs.sh` contains the function `xvg2png` which calls the GNUPlot script `xvg2png.gp`. For usage run `xvg2png --help`.

#### Wrapper for "amp_stats.py"

A wrapper function with logging functionality exists as:

`dnaBondStats -l $LOGNUM`

### Prepare PDBs for Gromacs

`pdb_mod.sh`

Tools to modify PDB's for use within Gromacs.

Available methods:

* Summary of a PDB
* Find a residue entry in a given force field
* List all instances of an atom name in a PDB
* Replace all entries with a given atom name with another in a PDB
* Remove a chain from a PDB

For detailed usage information run `pdb_mod.sh --help` and see the [wiki](https://gitlab.eps.surrey.ac.uk/mw00368/GromacsOnEUREKA/-/wikis/home).

### Analysis using AMP

The python script `ase_stats.py` uses [AseMolPlot](https://github.com/mwinokan/AseMolPlot) to perform analysis and produce graphs.

Currently it supports:

* Converting a trajectory (.pdb from gmx trjconv) to an ASE Trajectory (.traj)
* Plotting lengths of hydrogen bond interactions between matching DNA base pairs.

For minimal instructions run:

`amp_stats.py -h`

### Example with 3PJR

`run_3pjr.sh`

Example MD script.
