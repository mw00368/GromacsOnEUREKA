#!/bin/bash

if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
	echo "Please source this script for it to work!"
	exit 2
fi

NEW=0
NOPURGE=0
GPU=0
ARCHER=0
VERBOSITY=1

source $MWSHPATH/out.sh

while test $# -gt 0; do
  case "$1" in
  	-h|-help|--help|-usage|--usage)
      echo -e $colBold"Methods for "$colFunc"load_gmx.sh"$colClear":"
      echo
      echo -e $colUnderline"Load Gromacs 2018$colClear:"
      echo -e $colFunc"load_gmx.sh"$colClear
      echo
      echo -e $colUnderline"Load Gromacs 2020$colClear:"
      echo -e $colFunc"load_gmx.sh"$colClear$colArg" -2020"$colClear
      echo
      echo -e $colUnderline"Load Gromacs 2020 (GPU)$colClear:"
      echo -e $colFunc"load_gmx.sh"$colClear$colArg" -gpu"$colClear
      echo
      echo -e $colUnderline"Additional Flags (GPU)$colClear:"
      echo -e $colArg" --no-purge"$colClear" Don't purge the modules. "
      echo -e $colArg" --quiet"$colClear" Suppress console output. "
      echo
      return 0
      ;;
    -2020|--2020)
      shift
      NEW=1
      ;;
    -gpu|--gpu)
      shift
      GPU=1
      ;;
    -np|--no-purge)
      shift
      NOPURGE=1
      ;;
    -q|--quiet)
      shift
      VERBOSITY=0
      ;;
    -a|--archer)
      shift
      ARCHER=1
      NOPURGE=1
      ;;
    *)
      warningOut "Unrecognised CLI flag: $colArg$1"
      break
      ;;
  esac
done

export GMXLIB=$HOME"/gmx_ff"

if [ $ARCHER -eq 1 ] ; then

	if [ $NOPURGE -eq 0 ] ; then
		module purge              # Unload all currently loaded modules to reduce chance of conflicts. 
	fi

  VERSION=gromacs/2021.1+cp2k

  # load cp2k-gromacs
  module use /work/y07/shared/archer2-lmod/training/
  module load gromacs/2021.1+cp2k
  EXE=gmx_cp2k
  export GMXLIB=$WORK"/gmx_ff"

  ## $ module show gromacs/2021.1+cp2k
  # prepend_path("PATH","/work/y07/shared/apps/core/gromacs/2021.1+cp2k/bin")
  # setenv("GMXLIB","/work/y07/shared/apps/core/gromacs/2021.1+cp2k/share/top")
  # setenv("CP2K_DATA_DIR","/work/y07/shared/apps/core/cp2k/cp2k-8.1/data")
  # family("gromacs")

elif [ $GPU -eq 1 ] ; then

	if [ $NOPURGE -eq 0 ] ; then
		module purge              # Unload all currently loaded modules to reduce chance of conflicts. 
	fi
	
	VERSION=gromacs/2020-GPU

	module load $VERSION  # Load the GROMACS module and its dependencies.
	source /opt/pkg/apps/gromacs/2020/gromacs-install/bin/GMXRC.bash # Set up the GROMACS environment variables.
	EXE=gmx

elif [ $NEW -eq 1 ] ; then

  if [ $NOPURGE -eq 0 ] ; then
    module purge              # Unload all currently loaded modules to reduce chance of conflicts. 
  fi
  
  VERSION=gromacs/2020.3-mpi

  module load $VERSION  # Load the GROMACS module and its dependencies.
  source /opt/pkg/apps/gromacs/2020-mpi/gromacs-install/bin/GMXRC.bash # Set up the GROMACS environment variables.
  EXE=gmx_mpi

else

	if [ $NOPURGE -eq 0 ] ; then
		module purge              # Unload all currently loaded modules to reduce chance of conflicts. 
	fi
	
	VERSION=gromacs/2018

	module load $VERSION  # Load the GROMACS module and its dependencies.
	source /opt/proprietary-apps/gromacs/2018/bin/GMXRC.bash # Set up the GROMACS environment variables.
	EXE=gmx_mpi

fi	

export GMXEXE=$EXE

if [ $VERBOSITY -gt 0 ] ; then
	headerOut "$colFunc$VERSION$colClear$colBold is now available as $EXE"
fi

return 0
