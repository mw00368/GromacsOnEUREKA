#!/usr/bin/env python

import argparse

import mout # https://github.com/mwinokan/MPyTools
import mcol # https://github.com/mwinokan/MPyTools
# import mplot # https://github.com/mwinokan/MPyTools

import asemolplot as amp # https://github.com/mwinokan/AseMolPlot

import os

##########################################################################

argparser = argparse.ArgumentParser(description='Convert traj Coordinates to a Gromacs suitable PDB')

argparser.add_argument("-i","--input",help="Input file")
argparser.add_argument("-o","--output",help="Output file")

args = argparser.parse_args()
  
##########################################################################

if args.input is None:
  mout.errorOut("No input file specified",fatal=True)

if args.output is None:
  pre, ext = os.path.splitext(args.input)
  outfile = pre + ".pdb"
  mout.warningOut("No output file specified, defaulted to "+mcol.file+outfile)
else:
  outfile = args.output

try:
  atoms = amp.read(args.input)
except:
  mout.errorOut("Could not read file!",fatal=True)

amp.write("__temp__.pdb",atoms)

os.system("grep -v MODEL __temp__.pdb | grep -v ENDMDL > "+outfile)

os.system("rm __temp__*")

