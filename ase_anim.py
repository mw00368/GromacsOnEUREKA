from ase import io

# import mcol              # https://github.com/mwinokan/MPyTools
# import mout              # https://github.com/mwinokan/MPyTools
import asemolplot as amp # https://github.com/mwinokan/AseMolPlot

import sys
import os

filename=sys.argv[1]
subdirectory=sys.argv[2]
interval=int(sys.argv[3])

# print(filename)
# print(subdirectory)
# print(interval)

custom_gifstyle = amp.styles.gif_standard
custom_gifstyle['fps'] = 10
# custom_gifstyle["background"] = "white"

# atoms = io.read(filename,index=-1)
# amp.makeImage('test',atoms,verbosity=0,**amp.styles.standard)

# amp.makeImages(filename,
#                interval=interval,
#                verbosity=3,
#                **custom_style)
#                # subdirectory=subdirectory,

# custom_style = amp.styles.standard_cell
# custom_style['canvas_width'] = 600
# custom_style['canvas_height'] = 600
# custom_style['crop_yshift'] = 0
# custom_style['transparent'] = False

# amp.makeAnimation(filename,
#                   interval=interval,
#                   gifstyle=custom_gifstyle,
#                   verbosity=3,
#                   **custom_style)

custom_style = amp.styles.standard_cell
custom_style['canvas_width'] = 1200
custom_style['canvas_height'] = 1200
custom_style['crop_yshift'] = 0
custom_style['transparent'] = False

custom_style['crop_w'] = 1100
custom_style['crop_h'] = 1100
custom_style['crop_xshift'] = 50
custom_style['crop_yshift'] = 50

amp.makePovAnimation(filename,
                     subdirectory=subdirectory,
                     interval=interval,
                     gifstyle=custom_gifstyle,
                     verbosity=3,
                     **custom_style)
                     # useExisting=True,
