#!/usr/bin/env python3

import sys

assert len(sys.argv) == 3

from cairosvg import svg2png

with open(sys.argv[1], 'r') as f:
	file_content = f.read() # Read whole file in the file_content string print(file_content)

svg2png(bytestring=file_content,write_to=sys.argv[2])
