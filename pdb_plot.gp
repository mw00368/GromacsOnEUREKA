#!/usr/bin/env gnuplot -c

set term pngcairo enhanced dashed  font 'Arial,32' size 900,675  # 0.5 textwidth on letter paper

PDB=ARG1

if (PDB eq "") {
  print("Please pass an input file as a CLI argument")
  quit
}

PDB_FILENAME_LEN=strlen(PDB)
KEY=PDB[:PDB_FILENAME_LEN-4]

set output KEY.'.png'

# set datafile commentschars ''
set size ratio -1

unset xtics
unset ytics
unset border

NUM_COLS=system(sprintf("grep 'ATOM' %s 2> /dev/null | head -n1 | wc -w",PDB))
NUM_MODELS=system(sprintf("grep 'MODEL' %s | tail -n1 | awk '{print $2}'",PDB))

set title PDB noenhanced

if (NUM_MODELS > 0) {
  system(sprintf("sed -e '1,/MODEL.*%s/ d' %s > __temp__",NUM_MODELS,PDB))
  if (NUM_MODELS > 1) {
    set title PDB."[-1]" noenhanced
  }
} else {
  system(sprintf("cp %s __temp__",PDB))
}

system("cat __temp__ | grep -v 'CRYST\|REMARK\|TITLE\|ENDMDL\|TER\|MODEL' > __temp__2")

if (NUM_COLS == 10 ) {
  system("awk '{print $3 \" \" $6 \" \" $7 \" \" $8 \" \" substr($3,1,1)}' __temp__2 > __temp__3")
}

if (NUM_COLS == 11 ) {
  plot \
         "__temp__2" u 6:(stringcolumn(11) eq 'H' ? $7 : 1/0 ) w p lt 10 pt 7 ps 4 lc 'gray80' notitle,\
         "__temp__2" u 6:(stringcolumn(11) eq 'C' ? $7 : 1/0 ) w p lt 10 pt 7 ps 12 lc 'gray50' notitle,\
         "__temp__2" u 6:(stringcolumn(11) eq 'N' ? $7 : 1/0 ) w p lt 10 pt 7 ps 10 lc 'skyblue' notitle,\
         "__temp__2" u 6:(stringcolumn(11) eq 'O' ? $7 : 1/0 ) w p lt 10 pt 7 ps 8 lc 'red' notitle,\
         "__temp__2" u 6:7:3 w labels font ',12' notitle
} else { if (NUM_COLS == 10) {
  plot \
         "__temp__3" u 2:(stringcolumn(5) eq 'H' ? $3 : 1/0 ) w p lt 10 pt 7 ps 4 lc 'gray80' notitle,\
         "__temp__3" u 2:(stringcolumn(5) eq 'C' ? $3 : 1/0 ) w p lt 10 pt 7 ps 12 lc 'gray50' notitle,\
         "__temp__3" u 2:(stringcolumn(5) eq 'N' ? $3 : 1/0 ) w p lt 10 pt 7 ps 10 lc 'skyblue' notitle,\
         "__temp__3" u 2:(stringcolumn(5) eq 'O' ? $3 : 1/0 ) w p lt 10 pt 7 ps 8 lc 'red' notitle,\
         "__temp__3" u 2:3:1 w labels font ',12' notitle
} else {
  print("Unsupported number of columns in PDB")
}}

system("rm __temp__*")
