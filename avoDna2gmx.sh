source $MWSHPATH/out.sh

EXPERT=0
ALTERNATE=0

while test $# -gt 0; do
  case "$1" in
    -u|--usage|-h|--help)
      echo -e $colBold"Usage for "$colFunc"avoDna2gmx.sh"$colClear":"
      echo
      echo -e "This script will attempt to modify a PDB containing DNA chains produced"
      echo -e "by Avogadro's DNA builder so that it plays ball with GROMACS' pdb2gmx,"
      echo -e "and matches the atom naming schemes in the CHARMM36 force fields."
      echo 
      echo -e $colFunc"avoDna2gmx.sh"$colClear$colArg" --input <PDB> [--output <PDB>] [-expert]"$colClear
      echo 
      echo -e $colArg"-i|--input"$colClear" Specify input"$colClear
      echo -e $colArg"-o|--output"$colClear" Specify output"$colClear
      echo -e $colArg"-x|--expert"$colClear" Hide warnings"$colClear
      exit
      ;;
    -i|--input)
      shift
      INPUT=$1
      shift
      ;;
    -o|--output)
      shift
      OUTPUT=$1
      shift
      ;;
    -x|--expert)
      shift
      EXPERT=1
      ;;
    -alt|--alternate)
      shift
      ALTERNATE=1
      ;;
    *)
      warningOut "Unrecognised CLI flag: $colArg$1"
      break
      ;;
  esac
done

if [[ -z $INPUT ]] ; then 
  errorOut "No input file specified."
  exit
fi
if [[ -z $OUTPUT ]] ; then 
  OUTPUT=$(echo "$INPUT" | sed 's/AVO/MOD/' | sed 's/avo/mod/')
  if [ $EXPERT -eq 0 ] ; then
    warningOut "No output file specified. Defaulted to $colFile""$OUTPUT"
  fi
fi
if [[ $INPUT == $OUTPUT ]] ; then 
  if [ $FORCE -eq 1 ] ; then
    if [ $EXPERT -eq 0 ] ; then
      warningOut "Overwriting $colFile"$INPUT"$colWarning!"
    fi
  else
    errorOut "Overwriting is not permitted. Select different input and output files. (ignore with -f)"
    exit
  fi
fi

if [[ -z $INPUT ]] ; then 
  errorOut "No input file specified."
  exit
fi

if [[ "$OUTPUT" == "__temp__" ]] ; then 
  errorOut "Choose a different output name."
  exit
fi

# PDB Summary
pdb_mod.sh --summary $INPUT
pdb_mod.sh --summary $INPUT --filter A -rlo # Chain A
pdb_mod.sh --summary $INPUT --filter B -rlo # Chain B
echo ""

N3TER=0
N5TER=0
TER_NUM=0

# Deal with termini
headerOut "Renaming termini atoms:"
# 5 prime end
if [ $ALTERNATE -eq 1 ] ; then
  # If you want to use pdb2gmx to select termini
  pdb_mod.sh --replace "HTER" "H5T" -i $INPUT -o $OUTPUT -x
  RET=$?
else
  # For the DX5 residue definition in CHARMM
  pdb_mod.sh --remove-atom "HTER" -i $INPUT -o $OUTPUT -x
  RET=$?
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DAA1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DTA1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DCA1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DGA1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DAB1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DTB1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DCB1 -x
  pdb_mod.sh --remove-atom "OXT" -i $OUTPUT -f -o $OUTPUT -filter DGB1 -x

  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DAA1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DTA1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DCA1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DGA1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DAB1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DTB1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DCB1 -x
  pdb_mod.sh --remove-atom "O5T" -i $OUTPUT -f -o $OUTPUT -filter DGB1 -x

  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DAA1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DTA1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DCA1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DGA1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DAB1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DTB1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DCB1 -x
  pdb_mod.sh --remove-atom "O1P" -i $OUTPUT -f -o $OUTPUT -filter DGB1 -x

  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DAA1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DTA1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DCA1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DGA1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DAB1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DTB1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DCB1 -x
  pdb_mod.sh --remove-atom "O2P" -i $OUTPUT -f -o $OUTPUT -filter DGB1 -x
  
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DAA1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DTA1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DCA1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DGA1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DAB1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DTB1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DCB1 -x
  pdb_mod.sh --replace "P" "H5T" -i $OUTPUT -f -o $OUTPUT -filter DGB1 -x
fi
let "N5TER = N5TER + RET"
# 3 prime end
pdb_mod.sh --replace "OXT" "O5'" -i $OUTPUT -f -o $OUTPUT -x
pdb_mod.sh --replace "HCAP" "H3T" -i $OUTPUT -f -o $OUTPUT -x
RET=$?
let "N3TER = N3TER + RET"
let "TER_NUM = N5TER + N3TER"
echo ""

# Deal with backbone
headerOut "Renaming backbone atoms:"
pdb_mod.sh --replace "H5'1" "H5'" -i $OUTPUT -f -o $OUTPUT -x
pdb_mod.sh --replace "H5'2" "H5''" -i $OUTPUT -f -o $OUTPUT -x
pdb_mod.sh --replace "H2'1" "H2'" -i $OUTPUT -f -o $OUTPUT -x
pdb_mod.sh --replace "H2'2" "H2''" -i $OUTPUT -f -o $OUTPUT -x

echo ""
if [ $TER_NUM -ne 0 ] ; then
  headerOut "You will likely have to select $TER_NUM termini when using $colFunc""pdb2gmx"
  headerOut "$N5TER x $colArg""5TER"
  headerOut "$N3TER x $colArg""3TER"
  echo ""
fi

headerOut "Remember to rename terminal residues!"
echo ""

headerOut "Now try to run pdb2gmx:"
echo -e "$colFunc""source"$colClear" $colFile\$GROMAX/load_gmx.sh"$colClear
echo -e "$colFunc""source"$colClear" $colFile\$GROMAX/gmx_funcs.sh"$colClear
echo -e "$colFunc""gromax$colClear "$colFunc"pdb2gmx$colClear $colArg-inter -f $colFile$OUTPUT$colClear"

echo ""
